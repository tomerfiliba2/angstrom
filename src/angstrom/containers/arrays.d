module angstrom.containers.arrays;

import angstrom.lib.typing: CapacityType;

struct FixedArray(T, size_t capacity_) {
    enum capacity = capacity_;
    CapacityType!capacity length;
    T[capacity_] elems;

    @property T[] slice() pure nothrow @nogc {
        return elems[0 .. length];
    }
    void opOpAssign(string op: "~")(T item) pure nothrow @nogc {
        length++;
        elems[length-1] = item;
    }
}

struct TypedIndexArray(IDX, VAL) {
    VAL[] underlying;

    this(VAL[] underlying) pure nothrow @nogc {
        this.underlying = underlying;
    }
    ref VAL opIndex(IDX idx) pure nothrow @nogc {
        return underlying[idx.value];
    }
}













