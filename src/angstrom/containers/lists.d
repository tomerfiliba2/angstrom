module angstrom.containers.lists;


struct IntrusiveList(T, bool withLength=false) {
    T* head;
    static if (withLength) {
        size_t length;
    }

    @property bool empty() const pure nothrow @nogc {
        return head is null;
    }

    @property inout(T)* tail() inout pure nothrow @nogc {
        return head is null ? null : head.prev;
    }

    void prepend(T* elem) nothrow @nogc {
        assert (elem.next is null && elem.prev is null);
        if (head is null) {
            head = elem;
            head.next = head;
            head.prev = head;
        }
        else {
            auto p = head.prev;
            elem.next = head;
            elem.prev = p;
            head.prev = elem;
            p.next = elem;
            head = elem;
        }
        static if (withLength) length++;
    }

    void append(T* elem) nothrow @nogc {
        assert (elem.next is null && elem.prev is null);
        if (head is null) {
            head = elem;
            head.next = head;
            head.prev = head;
        }
        else {
            auto tail = head.prev;
            tail.next = elem;
            elem.prev = tail;
            elem.next = head;
            head.prev = elem;
        }
        static if (withLength) length++;
    }

    void remove(T* elem) nothrow @nogc {
        assert (elem.next !is null && elem.prev !is null);
        assert (head !is null);
        static if (withLength) assert (length > 0);
        if (head.next is head) {
            assert (elem is head);
            head.next = null;
            head.prev = null;
            head = null;
        }
        else {
            auto p = elem.prev;
            auto n = elem.next;
            p.next = n;
            n.prev = p;
            elem.next = null;
            elem.prev = null;
            if (head is elem) {
                head = n;
            }
        }
        static if (withLength) length--;
    }

    T* popHead() nothrow @nogc {
        assert (!empty);
        auto tmp = head;
        remove(head);
        return tmp;
    }

    void removeAll() {
        while (!empty) {
            popHead();
        }
        static if (withLength) assert(length == 0);
    }
}

struct IntrusiveQueue(T) {
    T* head;
    T* tail;

    @property bool empty() const pure nothrow @nogc {
        return head is null;
    }

    void append(T* elem) {
        assert (elem.next is null);
        if (head is null) {
            assert (tail is null);
            head = elem;
        }
        else {
            tail.next = elem;
        }
        tail = elem;
    }

    void prepend(T* elem) {
        assert (elem.next is null);
        if (head is null) {
            assert (tail is null);
            tail = elem;
        }
        else {
            elem.next = head;
        }
        head = elem;
    }

    T* popHead() {
        assert (head !is null);
        auto elem = head;
        head = elem.next;
        if (head is null) {
            tail = null;
        }
        elem.next = null;
        return elem;
    }

    void removeAll() {
        while (!empty) {
            popHead();
        }
    }
}



