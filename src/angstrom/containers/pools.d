module angstrom.containers.pools;


private struct GenericPool(T, uint N=0, bool refcounted=false, uint alignment=1) {
    private enum uint MAGIC = 0x9ea7baa5;

    align(8) struct _Elem {
        void[T.sizeof] data;
        uint magic;
        uint myIdx;
        uint nextFreeIdx;
        uint refcount;

        @property T* value() {
            return cast(T*)data.ptr;
        }
    }

    static if (alignment <= 1) {
        alias Elem = _Elem;
    }
    else {
        struct Elem {
            _Elem elem;
            alias elem this;
            ubyte[alignment - (_Elem.sizeof % alignment)] padding;
        }
    }

    enum uint invalidIdx = uint.max;
    private uint freeIdx;
    private uint _numUsed;

    static if (N == 0) {
        private Elem[] arr;

        void open(uint capacity, bool registerWithGC=true) {
            assert (N < invalidIdx);
        }
        void close() {
        }

        @property uint capacity() pure const nothrow @nogc {pragma(inline, true); return cast(uint)arr.length;}
    }
    else {
        static assert (N < invalidIdx);
        private Elem[N] arr;
        enum capacity = N;

        void open() {
            freeIdx = 0;
            _numUsed = 0;
            foreach(i, ref e; arr) {
                e.magic = MAGIC;
                e.myIdx = i;
                e.nextFreeIdx = i+1;
                e.refcount = 0;
            }
            arr[$-1].nextFreeIdx = invalidIdx;
        }
    }

    @property uint numUsed() pure const nothrow @nogc {pragma(inline, true); return _numUsed;}
    @property uint numAvailable() pure const nothrow @nogc {pragma(inline, true); return capacity - _numUsed;}

    T* tryAlloc() {
        if (freeIdx == invalidIdx) {
            return null;
        }
        assert (_numUsed < capacity);
        Elem* e = &arr[freeIdx];
        assert (e.magic == MAGIC && e.refcount == 0);
        freeIdx = e.nextFreeIdx;
        _numUsed++;
        e.nextFreeIdx = invalidIdx;
        e.refcount = 1;
        T* obj = e.value;
        static if (__traits(hasMember, T, "_poolElemInit")) {
            obj._poolElemInit();
        }
        else {
            *obj = T.init;
        }
        return obj;
    }
    T* alloc() {
        auto obj = tryAlloc();
        assert(obj !is null);
        return obj;
    }

    private Elem* _getElem(T* obj) {
        assert (_numUsed > 0);
        assert (cast(void*)obj >= cast(void*)arr.ptr && cast(void*)obj < cast(void*)(arr.ptr + arr.length));
        Elem* e = cast(Elem*)((cast(void*)obj) - _Elem.data.offsetof);
        assert (e.refcount > 0 && e.magic == MAGIC);
        return e;
    }
    private void _release(Elem* e) {
        assert (_numUsed > 0);
        e.nextFreeIdx = freeIdx;
        e.refcount = 0;
        freeIdx = e.myIdx;
        static if (__traits(hasMember, T, "_poolElemRelease")) {
            e.value._poolElemRelease();
        }
        else {
            destroy(*e.value);
        }
        _numUsed--;
    }

    static if (refcounted) {
        void incref(T* obj) {
            auto e = _getElem(obj);
            e.refcount++;
        }
        void decref(T* obj) {
            auto e = _getElem(obj);
            if (e.refcount == 1) {
                _release(e);
            }
            else {
                e.refcount--;
            }
        }
    }
    else {
        void release(T* obj) {pragma(inline, true);
            _release(_getElem(obj));
        }
    }

    uint indexOf(T* obj) {
        return _getElem(obj).myIdx;
    }
    T* fromIndex(uint idx) {
        if (idx == invalidIdx) {
            return null;
        }
        assert (idx < capacity);
        Elem* e = &arr[idx];
        return e.refcount == 0 ? null : e.value;
    }
}


alias StaticArrayPool(T, uint N, bool refcounted=false, uint alignment=1) = GenericPool!(T, N, refcounted, alignment);
alias Pool(T, bool refcounted=false, uint alignment=1) = GenericPool!(T, 0, refcounted, alignment);














