module angstrom.lib.asserts;

void ASSERT(string fmt, string mod=__MODULE__, size_t line=__LINE__, T...)(bool cond, scope auto ref lazy T args) @trusted pure nothrow @nogc {
    if (!cond) {
        scope void deleate() dg = (){
            assert(false);
        };
        (cast(void delegate() nothrow pure @nogc)dg)();
    }
}

version (assert) {
    enum DBG_ASSERT_ENABLED = true;
    alias DBG_ASSERT = ASSERT;
}
else {
    enum DBG_ASSERT_ENABLED = false;
    void DBG_ASSERT(string fmt, string mod=__MODULE__, size_t line=__LINE__, T...)(bool cond, scope auto ref lazy T args) @trusted pure nothrow @nogc {pragma(inline, true);}
}

