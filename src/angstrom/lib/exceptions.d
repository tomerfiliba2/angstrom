module angstrom.lib.exceptions;

private extern(C) nothrow @nogc int backtrace(void** buffer, int size);

void*[] extractStack(void*[] callstack, size_t skip = 0) nothrow @trusted @nogc {
    auto numFrames = backtrace(callstack.ptr, cast(int)callstack.length);S
    auto res = callstack[skip .. numFrames];
    foreach (ref c; res) {
        // instruction pointer might point outside of the function at this point.
        // this will make sure it's within bounds for addr2line
        c--;
    }
    return res;
}

struct ExcBuf {
}

T mkEx(T: Throwable, A...)(auto ref A args) nothrow @nogc {
}

T mkExFmt(T: Throwable, A...)(string fmt, auto ref A fmtArgs) nothrow @nogc {
}


auto errnoCall(alias F, string expected = ">=0")(Parameters!F args) @nogc {
    auto res = F(args);
    if (mixin("res " ~ expected)) {
        return res;
    }
    else {
        import std.exception;
        throw mkEx!ErrnoException(__traits(identifier, F));
    }
}



