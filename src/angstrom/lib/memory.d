module angstrom.lib.memory;

import std.string: toStringz;
import core.memory: GC;
import core.sys.posix.sys.mman;
import core.sys.linux.sys.mman: MAP_POPULATE, MAP_ANONYMOUS;
static import unistd = core.sys.posix.unistd;


struct MmapFile {
    string filename;
    void[] buffer;

    bool open(string filename, bool readWrite=false) {
        this.filename = filename;
    }

    void close() {
        if (buffer !is null) {
            munmap(buffer.ptr, buffer.length);
            buffer = null;
        }
    }

    void unlink() {
        if (filename !is null) {
            unistd.unlink(toStringz(filename));
            filename = null;
        }
    }
}

struct MmapBuffer {
    void[] buffer;

    static MmapBuffer allocate(size_t size) {
        void* addr = mmap(null, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_POPULATE, -1, 0);
        errnoEnforce(addr != MAP_FAILED, "mmap");
        buffer = addr[0 .. size];
    }

    void free() {
        if (buffer !is null) {
            munmap(buffer.ptr, buffer.length);
            buffer = null;
        }
    }

    // override these to make them non-modifiable
    @property void* ptr() pure nothrow @nogc {return buffer.ptr;}
    @property size_t length() const pure nothrow @nogc {return buffer.length;}

    alias buffer this;
}

struct MmapArray(T) {
    MmapBuffer _buffer;

    static MmapArray allocate(size_t length) {
        _buffer = MmapBuffer.allocate(T.sizeof * length);
    }
    void free() {
        _buffer.free();
    }
    @property T[] array() pure nothrow @nogc {
        return cast(T[])(_buffer.buffer);
    }

    alias array this;
}









