module angstrom.lib.time;

public import std.datetime: Clock, Duration, usecs, msecs, seconds;
import core.sys.posix.time;
import angstrom.arch.x86: readTSC;


struct TscTimePoint {
    enum zero = TscTimePoint(0);
    enum min = TscTimePoint(long.min);
    enum max = TscTimePoint(long.max);

    private __gshared static timespec initialKernelReading;
    private __gshared static long initialTscReading;
    private shared static long lastTsc;
    private shared static ulong softCounter;
    private shared static ulong refreshIntervalMask;
    __gshared static long cyclesPerSecond;

    long cycles;

    @property static TscTimePoint now() nothrow @nogc {
        return TscTimePoint(readTSC());
    }

    //
    // reading the TSC is expensive; usually a strict monotonically-increasing value is enough
    //
    @property static TscTimePoint softNow() nothrow @nogc {
        import core.atomic;
        ulong counter = atomicOp!"+="(softCounter, 1) - 1;
        if ((counter & refreshIntervalMask) == 0) {
            auto oldVal = lastTsc;
            auto newVal = readTSC();
            if (newVal > oldVal) {
                // we may fail to update the global variable here, but this will make sure we never go back
                cas(&lastTsc, oldVal, newVal);
            }
            return TscTimePoint(newVal);
        }
        else {
            // executing this function will take more than one cycle, so we can rest assured we will
            // never out-run a true TSC reading
            long tmp = atomicOp!"+="(lastTsc, 1);
            return TscTimePoint(val);
        }
    }

    static void setRefreshInterval(ulong interval) {
        import angstrom.lib.bits: intLog2;
        refreshIntervalMask = (1UL << intLog2(interval)) - 1;
    }

    static long toCycles(Duration dur) nothrow @nogc {
        long hns = dur.total!"hnsecs";
        return (hns / 10_000_000) * cyclesPerSecond + ((hns % 10_000_000) * cyclesPerSecond) / 10_000_000;
    }

    Duration toDuration() nothrow @nogc {
        return toDuration(cycles);
    }
    static Duration toDuration(ulong cycles) nothrow @nogc {
        return hnsecs((cycles / cyclesPerSecond) * 10_000_000 + ((cycles % cyclesPerSecond) * 10_000_000) / cyclesPerSecond);
    }
    auto diff(string units)(TscTimePoint rhs) nothrow @nogc {
        static if (units == "cycles") {
            return cycles - rhs.cycles;
        }
        static if (units == "usecs") {
            return (cycles - rhs.cycles) / (cyclesPerSecond / 1_000_000);
        }
        static if (units == "seconds") {
            return double(cycles - rhs.cycles) / cyclesPerSecond;
        }
    }

    shared static this() {
        import std.exception;
        import std.file: readText;
        import std.string;

        enforce(readText("/proc/cpuinfo").indexOf("constant_tsc") >= 0, "constant_tsc not supported");

        timespec sleepTime = {tv_sec: 0, tv_nsec: 1_000_000}; // 1 msec
        clock_gettime(CLOCK_MONOTONIC, &initialKernelReading);
        initialTscReading = readTSC();
        nanosleep(&sleepTime, null);

        adjustFrequency();
    }

    //
    // this function should be called periodically (every minute or so) to keep our frequency
    // on par with the kernel's
    //
    static void adjustFrequency() {
        import angstrom.logging;

        timespec t1;
        clock_gettime(CLOCK_MONOTONIC, &t1);
        long tscNow = readTSC();

        long nsecs = (t1.tv_sec - initialKernelReading.tv_sec) * 1_000_000_000UL + (t1.tv_nsec  - initialKernelReading.tv_nsec);
        long newCyclesPerSecond = cast(long)((tscNow - initialTscReading) / (nsecs / 1E9));

        INFO!"Adjusting #TSC frequency from %s to %s (diff=%s)"(cyclesPerSecond, newCyclesPerSecond,
            newCyclesPerSecond - cyclesPerSecond);
        cyclesPerSecond = newCyclesPerSecond;
    }
}




