module angstrom.logging.impl;

import angstrom.logging.types;

private enum useColors = false;

package void LOG(LogLevel level, string msg, string mod=__MODULE__, size_t line=__LINE__, T...)(auto ref T args) nothrow @nogc {
    static if (level >= minLogLevel) {
        scope void delegate() dg = () {
            import std.datetime;
            import std.string;
            import std.stdio: writefln, fflush, stdin;
            auto now = Clock.currTime();
            immutable shared static string[] colors = ["0;37", "1;34", "1;31"];

            if (useColors) {
                writefln("\x1b[1;30m%02d:%02d:%02d.%06d | %-10s:%4d | %04x | \x1b[%sm" ~ msg ~ "\x1b[0m",
                    now.hour, now.minute, now.second, now.fracSecs.total!"usecs", mod.split(".")[$-1], line, loggerId, colors[level], args);
            }
            else {
                writefln("%02d:%02d:%02d.%06d | %-10s:%4d | %04x | " ~ msg,
                    now.hour, now.minute, now.second, now.fracSecs.total!"usecs", mod.split(".")[$-1], line, loggerId, args);
            }
            stdin.flush();
        };
        (cast(void delegate() nothrow @nogc)dg)();
    }
}
