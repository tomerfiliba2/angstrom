module angstrom.logging;

import angstrom.logging.impl: LOG;
public import angstrom.logging.types: LogLevel;


void DEBUG(string msg, string mod=__MODULE__, size_t line=__LINE__, T...)(auto ref T args) nothrow @nogc {pragma(inline, true);
    LOG!(LogLevel.DEBUG, msg, mod, line, T)(args);
}
void INFO(string msg, string mod=__MODULE__, size_t line=__LINE__, T...)(auto ref T args) nothrow @nogc {pragma(inline, true);
    LOG!(LogLevel.INFO, msg, mod, line, T)(args);
}
void WARN(string msg, string mod=__MODULE__, size_t line=__LINE__, T...)(auto ref T args) nothrow @nogc {pragma(inline, true);
    LOG!(LogLevel.WARN, msg, mod, line, T)(args);
}
void ERROR(string msg, string mod=__MODULE__, size_t line=__LINE__, T...)(auto ref T args) nothrow @nogc {pragma(inline, true);
    LOG!(LogLevel.ERROR, msg, mod, line, T)(args);
}



