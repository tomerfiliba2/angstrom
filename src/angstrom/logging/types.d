module angstrom.logging.types;

enum LogLevel {
    DEBUG = 10,
    INFO = 20,
    WARN = 30,
    ERROR = 40,
    DISABLED = 100,
}

/* thread local */ uint loggerId;

enum minLogLevel = LogLevel.DEBUG;

