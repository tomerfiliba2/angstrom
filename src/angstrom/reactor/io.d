module angstrom.io;

import std.exception;
import std.stdint;
import std.conv: octal;
import std.traits;
import std.string;

public import core.sys.posix.sys.types: ssize_t, off_t;
import core.stdc.stdio: SEEK_SET, SEEK_CUR, SEEK_END;
static import errnomod = core.stdc.errno;
static import epollmod = core.sys.linux.epoll;
static import signalmod = core.sys.posix.signal;
static import signalfd = core.sys.linux.sys.signalfd;
static import unistd = core.sys.posix.unistd;
static import fcntl = core.sys.posix.fcntl;
static import arpa = core.sys.posix.arpa.inet;
static import statmod = core.sys.posix.sys.stat;
static import sockmod = core.sys.posix.sys.socket;
static import inotify = core.sys.linux.sys.inotify;
static import in_ = core.sys.posix.netinet.in_;
static import un_ = core.sys.posix.sys.un;
static import netdb = core.sys.posix.netdb;

import angstrom.lib;


private struct FileEntry {
private:
    enum uint INVALID_GENERATION = 0;
    __gshared static uint generationCounter = 1;
    __gshared static FileEntry[] fileTable;

    int fd = -1;
    uint generation;
    uint watchedMask;
    uint eventMask;
    void function(FileEntry*) eventHandler;
    void* opaque;
    static assert (this.sizeof == 32);

    @disable this(this);

    static void add(int fd, int* entryIdx, uint* generation) {
        assert (fd >= 0);
        if (fd >= fileTable.length) {
            fileTable.reserve((((fd+1 + 255) / 256) * 256)); // reserve in chunks of 256
            fileTable.length = fd;
        }
        auto entry = &fileTable[fd];
        assert (entry.fd < 0);

        entry.fd = fd;
        entry.generation = generationCounter;
        assert (entry.generation != INVALID_GENERATION);
        generationCounter += 2; // make sure it never becomes 0 as it's our invalid value
        entry.watchedMask = 0;
        entry.eventMask = 0;
        entry.eventHandler = null;
        entry.opaque = null;

        *entryIdx = fd;
        *generation = entry.generation;
    }

    static FileEntry* get(int entryIdx, uint generation) nothrow @nogc {pragma(inline, true);
        if (entryIdx >= 0 && entryIdx < fileTable.length && fileTable[entryIdx].generation == generation && generation != INVALID_GENERATION) {
            assert (fileTable[entryIdx].fd == entryIdx);
            return &fileTable[entryIdx];
        }
        else {
            return null;
        }
    }

    private void setHandler(void function(FileEntry*) fn, void* opaque) nothrow @nogc {
        assert (fd >= 0 && eventHandler is null);
        eventHandler = fn;
        opaque = opaque;
    }
    private void clearHandler() pure nothrow @nogc {
        eventHandler = null;
        opaque = null;
    }

    private void _markClosed() {
        if (eventHandler !is null) {
            eventHandler(&this);
        }
        eventMask = 0;
        watchedMask = 0;
        eventHandler = null;
        opaque = null;
        fd = -1;
        generation = INVALID_GENERATION;
    }

    static void close(int* entryIdx, uint* generation) {
        if (auto entry = get(*entryIdx, *generation)) {
            auto tmpFD = entry.fd;
            entry._markClosed();
            errnoEnforce(unistd.close(tmpFD), "close");
        }
        *entryIdx = -1;
        *generation = INVALID_GENERATION;
    }

    bool invokeHandler() {
        if (eventHandler is null || (watchedMask & eventMask) == 0) {
            return false;
        }
        eventHandler(&this);
        return true;
    }
}

align(1) struct FileHandle {
align(1):
    int entryIdx = -1;
    uint generation = FileEntry.INVALID_GENERATION;
    enum invalid = FileHandle.init;

    static assert (this.sizeof == ulong.sizeof);

    private this(int fd) {
        FileEntry.add(fd, &entryIdx, &generation);
    }
    private this(const FileEntry* entry) {
        entryIdx = entry.fd;
        generation = entry.generation;
    }

    void close() {pragma(inline, true);
        FileEntry.close(&entryIdx, &generation);
    }
    @property bool closed() const nothrow @nogc {pragma(inline, true);
        return FileEntry.get(entryIdx, generation) is null;
    }
    @property FileEntry* getEntry() const nothrow @nogc {pragma(inline, true);
        auto entry = FileEntry.get(entryIdx, generation);
        assert (entry !is null);
        return entry;
    }
    @property int getFD() const nothrow @nogc {pragma(inline, true);
        return getEntry().fd;
    }

    auto call(alias F)(Parameters!F[1..$] args) if (is(Parameters!F[0] == int) && isSigned!(ReturnType!F)) {
        return F(getFD, args);
    }
    auto checkedCall(alias F, string cond = ">= 0")(Parameters!F[1..$] args) if (is(Parameters!F[0] == int) && isSigned!(ReturnType!F)) {
        auto rc = call!F(args);
        errnoEnforce(mixin("rc " ~ cond), __traits(identifier, F));
        return rc;
    }

    void suspendForIO(uint mask) {
        import angstrom.reactor.subsystems.fibers: FiberScheduler, FiberHandle;

        auto entry = getEntry();
        entry.eventMask &= ~mask;
        Epoller.register(entry, mask);

        static wakeCaller(FileEntry* entry) {
            // should we check if (eventMask & mask) != 0?
            // won't add much safety since there can only be a single handler at a given time
            FiberScheduler._resumeFiberHandle(*(cast(FiberHandle*)entry.opaque));
            entry.clearHandler();
        }

        entry.setHandler(&wakeCaller, *(cast(void**)FiberScheduler.thisFiberHandle));
        scope(failure) entry.clearHandler();
        FiberScheduler._suspendThisFiber();

        if (closed) {
            throw new ErrnoException("FileHandle had been closed while fiber was suspended", errnomod.EBADF);
        }
    }

    auto yieldCall(alias F)(uint mask, Parameters!F[1..$] args) {
        while (true) {
            auto entry = getEntry();
            auto rc = F(entry.fd, args);
            if (rc < 0) {
                if (errnomod.errno == errnomod.EAGAIN) {
                    suspendForIO(mask);
                }
                else {
                    errnoEnforce(errnomod.errno == errnomod.EINTR, __traits(identifier, F));
                }
            }
            return rc;
        }
    }
    auto yieldRead(alias F)(Parameters!F[1..$] args) if (is(Parameters!F[0] == int) && isSigned!(ReturnType!F)) {pragma(inline, true);
        return yieldCall!F(epollmod.EPOLLIN | epollmod.EPOLLRDHUP, args);
    }
    auto yieldWrite(alias F)(Parameters!F[1..$] args) if (is(Parameters!F[0] == int) && isSigned!(ReturnType!F)) {pragma(inline, true);
        return yieldCall!F(epollmod.EPOLLOUT, args);
    }

    FileHandle dup() {
        int fd2 = call!(unistd.dup)();
        errnoEnforce(fd2 >= 0, "dup");
        return FileHandle(fd2);
    }
    FileHandle dupTo(int destFD) {
        assert (destFD >= 0);
        int fd2 = call!(unistd.dup2)(destFD);
        errnoEnforce(fd2 == destFD, "dup");
        if (destFD < FileEntry.fileTable.length) {
            // dup2 will (implicitly) close fd2 if it were used
            FileEntry.fileTable[destFD]._markClosed();
        }
        return FileHandle(destFD);
    }

    void stat(statmod.stat_t* statRes) {
        checkedCall!(statmod.fstat)(statRes);
    }

    static void iterOpenHandles(scope void delegate(FileHandle) dg) {
        foreach(ref entry; FileEntry.fileTable) {
            if (entry.fd >= 0) {
                dg(FileHandle(&entry));
            }
        }
    }
}


struct File {
    //
    // note: files can't be registered for epoll
    //
    enum DEFAULT_MODE = octal!666; // same as libc's fopen

    FileHandle handle;
    alias handle this;

    private this(int fd) {
        handle = FileHandle(fd);
    }

    static File open(const(char)* fn, int flags, int mode = DEFAULT_MODE) {
        int fd = fcntl.open(fn, flags, mode);
        errnoEnforce(fd >= 0, "open");
        return File(fd);
    }
    static File open(string fn, int flags, int mode = DEFAULT_MODE) {
        return File.open(fn.toStringz, flags, mode);
    }
    static File open(string flagsStr)(string fn, int mode = DEFAULT_MODE) {
        int flags;
        static if (flagsStr == "r")       {flags = fcntl.O_RDONLY;}
        else static if (flagsStr == "w")  {flags = fcntl.O_WRONLY | fcntl.O_CREAT | fcntl.O_TRUNC;}
        else static if (flagsStr == "a")  {flags = fcntl.O_WRONLY | fcntl.O_CREAT | fcntl.O_APPEND;}
        else static if (flagsStr == "r+") {flags = fcntl.O_RDWR;}
        else static if (flagsStr == "w+") {flags = fcntl.O_RDWR   | fcntl.O_CREAT | fcntl.O_TRUNC;}
        else static if (flagsStr == "a+") {flags = fcntl.O_WRONLY | fcntl.O_CREAT | fcntl.O_APPEND;}
        else {
            static foreach(fl; flagsStr.split("|")) {
                static assert (fl.startsWith("O_"));
                flags |= __traits(getMember, fcntl, fl.strip());
            }
        }
        return File.open(fn.toStringz, flags, mode);
    }

    off_t seek(off_t offset) {
        return handle.checkedCall!(unistd.lseek)(offset, SEEK_SET);
    }
    off_t seekFromCurrent(off_t offset) {
        return handle.checkedCall!(unistd.lseek)(offset, SEEK_CUR);
    }
    off_t seekFromEnd(off_t offset) {
        return handle.checkedCall!(unistd.lseek)(offset, SEEK_END);
    }
    off_t tell() {
        return handle.checkedCall!(unistd.lseek)(0, SEEK_CUR);
    }

    ssize_t read(void[] buf) {
        return handle.call!(unistd.read)(buf.ptr, buf.length);
    }
    ssize_t write(const(void)[] buf) {
        return handle.call!(unistd.write)(buf.ptr, buf.length);
    }
}

struct Pipe {
    FileHandle handle;
    alias handle this;

    private this(int fd) {
        handle = FileHandle(fd);
    }

    // open a `mkfifo` file
    static Pipe open(const(char)* fn, int flags, int mode = File.DEFAULT_MODE) {
        int fd = fcntl.open(fn, flags, mode);
        errnoEnforce(fd >= 0, "open");
        statmod.stat_t statRes;
        errnoEnforce(statmod.fstat(fd, &statRes) == 0);
        enforce(statmod.S_ISFIFO(statRes.st_mode), "not a FIFO file");
        return Pipe(fd);
    }
    static Pipe open(string fn, int flags, int mode = File.DEFAULT_MODE) {
        return Pipe.open(fn.toStringz, flags, mode);
    }
    static void openPair(ref Pipe readEnd, ref Pipe writeEnd) {
        int[2] fds;
        errnoEnforce(unistd.pipe(fds) == 0, "pipe");
        readEnd = Pipe(fds[0]);
        writeEnd = Pipe(fds[1]);
    }

    ssize_t read(void[] buf) {
        return handle.yieldRead!(unistd.read)(buf.ptr, buf.length);
    }
    ssize_t write(const(void)[] buf) {
        return handle.yieldWrite!(unistd.write)(buf.ptr, buf.length);
    }
}

struct IP4 {
    enum ADDRSTRLEN = arpa.INET_ADDRSTRLEN;

    enum any = IP4(0, 0, 0, 0);
    enum invalid = IP4(0, 0, 0, 0);
    enum broadcast = IP4(0xff, 0xff, 0xff, 0xff);

    ubyte[in_.in_addr_t.sizeof] bytes;

    this(ubyte a, ubyte b, ubyte c, ubyte d) nothrow @nogc {
        bytes = [a, b, c, d];
    }
    this(string str) nothrow @nogc {
        char[ADDRSTRLEN+1] strz;
        strz[0 .. str.length] = str;
        int res = arpa.inet_pton(sockmod.AF_INET, strz.ptr, bytes.ptr);
        assert (res == 1, str);
    }

    @property ref inout(in_.in_addr) as_in_addr() inout nothrow @nogc {
        return *(cast(inout(in_.in_addr)*)bytes.ptr);
    }

    /+auto toString() {
        char[ADDRSTRLEN+1] buf;
        auto p = arpa.inet_ntop(sockmod.AF_INET, bytes.ptr, buf.ptr, bytes.length);
        assert (p == buf.ptr);
        return buf;
    }+/
}

struct IP6 {
    enum ADDRSTRLEN = arpa.INET6_ADDRSTRLEN;

    enum invalid = IP6.init;

    static assert (in_.in6_addr.sizeof == 16);
    ubyte[16] bytes;

    this(ubyte[16] bytes) nothrow @nogc {
        this.bytes = bytes;
    }
    this(string str) {
        char[ADDRSTRLEN+1] strz;
        strz[0 .. str.length] = str;
        int res = arpa.inet_pton(sockmod.AF_INET6, strz.ptr, bytes.ptr);
        assert (res == 1, str);
    }

    @property ref inout(in_.in6_addr) as_in6_addr() inout nothrow @nogc {
        return *(cast(inout(in_.in6_addr)*)bytes.ptr);
    }

    /+auto toString() {
        char[ADDRSTRLEN+1] buf;
        auto p = arpa.inet_ntop(sockmod.AF_INET6, bytes.ptr, buf.ptr, bytes.length);
        assert (p == buf.ptr);
        return buf;
    }+/
}

struct IPPort {
    ushort portNBO;

    this(ushort portHBO) {
        portNBO = in_.htons(portHBO);
    }

    @property ushort hostOrder() const pure nothrow @nogc {
        return in_.ntohs(portNBO);
    }

    @property ref inout(in_.in_port_t) as_in_port() inout nothrow @nogc {
        return *(cast(inout(in_.in_port_t)*)&portNBO);
    }
}

struct SockAddr {
    enum maxLength = sun.sizeof;

    sockmod.socklen_t length = maxLength;
    union {
        sockmod.sa_family_t  family = sockmod.AF_UNSPEC;
        sockmod.sockaddr     sa;
        in_.sockaddr_in      sin4;
        in_.sockaddr_in6     sin6;
        un_.sockaddr_un      sun;
    }

    this(IP4 ip, IPPort port) {
        length = sin4.sizeof;
        sin4.sin_family = sockmod.AF_INET;
        sin4.sin_port = port.as_in_port;
        sin4.sin_addr = ip.as_in_addr;
    }
    this(IP6 ip, IPPort port) {
        length = sin6.sizeof;
        sin6.sin6_family = sockmod.AF_INET6;
        sin6.sin6_port = port.as_in_port;
        sin6.sin6_addr = ip.as_in6_addr;
    }
    static SockAddr unixDomain(string path) {
        SockAddr sa;
        assert (path.length < sa.sun.sun_path.length);
        sa.length = sun.sizeof;
        sa.sun.sun_family = sockmod.AF_UNIX;
        sa.sun.sun_path[0 .. path.length] = cast(byte[])path;
        sa.sun.sun_path[path.length] = 0;
        return sa;
    }

    @property inout(sockmod.sockaddr)* as_sockaddr() inout nothrow @nogc {
        return &sa;
    }
}

struct BaseSocket {
    FileHandle handle;
    alias handle this;

    private this(int fd) {
        handle = FileHandle(fd);
    }
    this(int domain, int type, int protocol) {
        int fd = sockmod.socket(domain, type, protocol);
        errnoEnforce(fd >= 0, "socket() failed");
        handle = FileHandle(fd);
    }

    void bind(const ref SockAddr sa) {
        handle.checkedCall!(sockmod.bind)(sa.as_sockaddr, sa.length);
    }
    void getName(ref SockAddr sa) {
        sa.length = sa.maxLength;
        handle.checkedCall!(sockmod.getsockname)(sa.as_sockaddr, &sa.length);
    }
    void getOpt(int level, int optname, void *optval, sockmod.socklen_t *optlen) {
        handle.checkedCall!(sockmod.getsockopt)(level, optname, optval, optlen);
    }
    T getOpt(T)(int level, int optname) {
        T optval;
        sockmod.socklen_t optlen = T.sizeof;
        handle.checkedCall!(sockmod.getsockopt)(level, optname, &optval, &optlen);
        assert (optlen == T.sizeof);
        return optval;
    }
    void setOpt(int sockfd, int level, int optname, const(void)* optval, sockmod.socklen_t optlen) {
        handle.checkedCall!(sockmod.setsockopt)(level, optname, optval, optlen);
    }
    void setOpt(T)(int sockfd, int level, int optname, auto ref const T optval) {
        handle.checkedCall!(sockmod.setsockopt)(level, optname, &optval, T.sizeof);
    }
}

struct ListenerSocket {
    BaseSocket handle;
    alias handle this;

    private this(int fd) {
        handle = BaseSocket(fd);
    }
    this(int domain, int type, int protocol) {
        handle = BaseSocket(domain, type, protocol);
    }

    static ListenerSocket listen(const ref SockAddr sockAddr, int type, int protocol) {
        auto sock = ListenerSocket(sockAddr.family, type, protocol);
        sock.checkedCall!(sockmod.bind)(sockAddr.as_sockaddr, sockAddr.length);
        sock.checkedCall!(sockmod.listen)(10);
        return sock;
    }

    static ListenerSocket listenTCP(const ref SockAddr sockAddr) {
        return listen(sockAddr, sockmod.SOCK_STREAM, in_.IPPROTO_TCP);
    }
    ListenerSocket listenUNIX(string path) {
        return ListenerSocket(-1);
    }

    ConnectedSocket accept(ref SockAddr sa) {
        int fd = handle.yieldRead!(sockmod.accept)(sa.as_sockaddr(), &sa.length);
        return ConnectedSocket(fd);
    }
}

struct ConnectedSocket {
    BaseSocket handle;
    alias handle this;

    private this(int fd) {
        handle = BaseSocket(fd);
    }
    this(int domain, int type, int protocol) {
        handle = BaseSocket(domain, type, protocol);
    }

    static ConnectedSocket connect(const ref SockAddr sockAddr, int type, int protocol) {
        auto sock = ConnectedSocket(sockAddr.family, type, protocol);
        scope(failure) sock.close();

        auto rc = sock.call!(sockmod.connect)(sockAddr.as_sockaddr, sockAddr.length);
        if (rc != 0) {
            errnoEnforce(errnomod.errno == errnomod.EINPROGRESS, "connect() failed");
            sock.suspendForIO(epollmod.EPOLLOUT);

            errnomod.errno = sock.getOpt!int(sockmod.SOL_SOCKET, sockmod.SO_ERROR);
            errnoEnforce(errnomod.errno == 0, "connect() failed");
        }
        return sock;
    }

    static ConnectedSocket connectUNIX(string path) {
        auto sa = SockAddr.unixDomain(path);
        return connect(sa, sockmod.SOCK_STREAM, 0);
    }

    static ConnectedSocket connectTCP(const ref SockAddr sockAddr) {
        return connect(sockAddr, sockmod.SOCK_STREAM, in_.IPPROTO_TCP);
    }

    static void connectPair(ref ConnectedSocket[2] pair, int sockType = sockmod.SOCK_STREAM) {
        int[2] fds;
        errnoEnforce(sockmod.socketpair(sockmod.AF_UNIX, sockmod.SOCK_STREAM, 0, fds));
        pair[0] = ConnectedSocket(fds[0]);
        pair[1] = ConnectedSocket(fds[1]);
    }

    void shutdownRead() {
        handle.checkedCall!(sockmod.shutdown)(sockmod.SHUT_RD);
        Epoller.unregisterRead(handle);
    }
    void shutdownWrite() {
        handle.checkedCall!(sockmod.shutdown)(sockmod.SHUT_WR);
        Epoller.unregisterWrite(handle);
    }
    void shutdown() {
        handle.checkedCall!(sockmod.shutdown)(sockmod.SHUT_RDWR);
        Epoller.unregisterReadWrite(handle);
    }

    void getPeerName(ref SockAddr sa) {
        sa.length = sa.maxLength;
        handle.checkedCall!(sockmod.getpeername)(sa.as_sockaddr(), &sa.length);
    }

    ssize_t send(const(void)[] buf, int flags = 0) {
        return handle.yieldWrite!(sockmod.send)(buf.ptr, buf.length, flags);
    }
    ssize_t recv(void[] buf, int flags = 0) {
        return handle.yieldRead!(sockmod.recv)(buf.ptr, buf.length, flags);
    }
}

struct DatagramSocket {
    BaseSocket handle;
    alias handle this;

    private this(int fd) {
        handle = BaseSocket(fd);
    }
    this(int domain, int type, int protocol) {
        handle = BaseSocket(domain, type, protocol);
    }

    DatagramSocket openUDP() {
        return DatagramSocket(sockmod.AF_INET, sockmod.SOCK_DGRAM, in_.IPPROTO_UDP);
    }
    DatagramSocket openUDP(const ref SockAddr sockAddr) {
        auto sock = DatagramSocket(sockAddr.family, sockmod.SOCK_DGRAM, in_.IPPROTO_UDP);
        scope(failure) sock.close();
        sock.bind(sockAddr);
        return sock;
    }

    void connect(const ref SockAddr sockAddr, int type, int protocol) {
        handle.checkedCall!(sockmod.connect)(sockAddr.as_sockaddr, sockAddr.length);
    }
    ssize_t send(const(void)[] buf, int flags = 0) {
        return handle.yieldWrite!(sockmod.send)(buf.ptr, buf.length, flags);
    }
    ssize_t recv(void[] buf, int flags = 0) {
        return handle.yieldRead!(sockmod.recv)(buf.ptr, buf.length, flags);
    }

    ssize_t sendTo(const(void)[] buf, const ref SockAddr sockAddr, int flags = 0) {
        return handle.yieldWrite!(sockmod.sendto)(buf.ptr, buf.length, flags, sockAddr.as_sockaddr, sockAddr.length);
    }
    ssize_t recvFrom(void[] buf, ref SockAddr sockAddr, int flags = 0) {
        sockAddr.length = sockAddr.maxLength;
        return handle.yieldRead!(sockmod.recvfrom)(buf.ptr, buf.length, flags, sockAddr.as_sockaddr, &sockAddr.length);
    }
}

struct Epoller {
__gshared static:
    private FileHandle epHandle;

    private void _open() {
        int fd = epollmod.epoll_create1(epollmod.EPOLL_CLOEXEC);
        errnoEnforce(fd >= 0, "epoll_create1");
        epHandle = FileHandle(fd);
    }
    void close() {
        epHandle.close();
    }
    @property bool closed() nothrow @nogc {
        return epHandle.closed;
    }

    void register(FileEntry* entry, uint addMask) {
        uint newMask = entry.watchedMask | addMask;
        if (newMask == entry.watchedMask) {
            return;
        }
        if (entry.watchedMask == 0) {
            // first time adding this FD, make it nonblocking
            auto flags = fcntl.fcntl(entry.fd, fcntl.F_GETFL);
            errnoEnforce(flags >= 0, "fcntl(get) failed");
            if ((flags & fcntl.O_NONBLOCK) == 0) {
                errnoEnforce(fcntl.fcntl(entry.fd, fcntl.F_SETFL, flags | fcntl.O_NONBLOCK) != -1, "fcntl(O_NONBLOCK) failed");
            }
        }
        if (epHandle.closed) {
            _open();
        }

        epollmod.epoll_event evt = {
            events: newMask | epollmod.EPOLLET,
            data: {u64: (ulong(entry.fd) << 32) | ulong(entry.generation)},
        };
        epHandle.checkedCall!(epollmod.epoll_ctl)(entry.watchedMask == 0 ? epollmod.EPOLL_CTL_ADD : epollmod.EPOLL_CTL_MOD,
            entry.fd, &evt);
        entry.watchedMask = newMask;
    }

    void register(FileHandle handle, uint addMask) {
        register(handle.getEntry(), addMask);
    }

    void unregister(FileEntry* entry, uint removeMask) {
        uint newMask = entry.watchedMask & ~removeMask;
        if (newMask == entry.watchedMask || entry.watchedMask == 0) {
            return;
        }
        assert (!epHandle.closed);
        epollmod.epoll_event evt = {
            events: newMask | epollmod.EPOLLET,
            data: {u64: (ulong(entry.fd) << 32) | ulong(entry.generation)},
        };
        epHandle.checkedCall!(epollmod.epoll_ctl)(newMask == 0 ? epollmod.EPOLL_CTL_DEL : epollmod.EPOLL_CTL_MOD,
            entry.fd, &evt);
        entry.watchedMask = newMask;
    }

    void unregister(FileHandle handle, uint removeMask) {
        unregister(handle.getEntry(), removeMask);
    }

    void registerRead(FileHandle handle) {register(handle, epollmod.EPOLLIN | epollmod.EPOLLRDHUP);}
    void registerWrite(FileHandle handle) {register(handle, epollmod.EPOLLOUT);}
    void unregisterRead(FileHandle handle) {unregister(handle, epollmod.EPOLLIN | epollmod.EPOLLRDHUP);}
    void unregisterWrite(FileHandle handle) {unregister(handle, epollmod.EPOLLOUT);}
    void unregisterReadWrite(FileHandle handle) {unregister(handle, epollmod.EPOLLIN | epollmod.EPOLLRDHUP | epollmod.EPOLLOUT);}

    void waitAndDispatchEvents(int msecs) {
        if (epHandle.closed) {
            _open();
        }
        epollmod.epoll_event[64] events = void;
        int count = epHandle.checkedCall!(epollmod.epoll_wait)(events.ptr, events.length, msecs);
        foreach(const ref evt; events[0 .. count]) {
            auto entry = FileEntry.get(cast(int)(evt.data.u64 >> 32), cast(uint)evt.data.u64);
            entry.eventMask |= evt.events;
            if (!entry.invokeHandler()) {
                //DEBUG!"spurious wakeup"();
                unregister(FileHandle(entry), entry.eventMask & ~entry.watchedMask);
                entry.eventMask = 0;
            }
        }
    }
}

struct Inotify {
    private FileHandle handle;

    void close() {
        handle.close();
    }
    @property bool closed() nothrow @nogc {
        return handle.closed;
    }

    int watch(string path, uint mask) {
        if (handle.closed) {
            int fd = inotify.inotify_init();
            errnoEnforce(fd >= 0, "inotify_init");
            handle = FileHandle(fd);
        }

        return handle.checkedCall!(inotify.inotify_add_watch)(path.toStringz, mask);
    }

    int watch(string flags)(string path) {
        uint mask;
        static foreach(fl; flags.split("|")) {
            static assert (fl.startsWith("IN_"));
            mask |= __traits(getMember, inotify, fl.strip());
        }
        return watch(path, mask);
    }

    int watchDir(string path) {
        return watch!"IN_CREATE|IN_DELETE|IN_DELETE_SELF"(path);
    }
    int watchFile(string path) {
        return watch!"IN_ACCESS|IN_MODIFY|IN_DELETE_SELF|IN_MOVE_SELF"(path);
    }

    void unwatch(int wd) {
        assert (!handle.closed);
        handle.checkedCall!(inotify.inotify_rm_watch)(wd);
    }

    static struct InotifyEvent {
        int    wd;
        uint   mask;
        uint   cookie;
        char[] name;
    }

    static struct EventsRange {
        void[] buf;
        InotifyEvent front;

        @property bool empty() const pure nothrow @nogc {
            return buf.length == 0;
        }
        void popFront() nothrow @nogc {
            assert (buf.length >= inotify.inotify_event.sizeof);
            inotify.inotify_event* evt = cast(inotify.inotify_event*)buf.ptr;
            assert (inotify.inotify_event.sizeof + evt.len <= buf.length);
            front = InotifyEvent(evt.wd, evt.mask, evt.cookie, evt.len == 0 ? null : fromStringz(evt.name.ptr));
            buf = buf[inotify.inotify_event.sizeof + evt.len .. $];
        }
    }

    auto yieldEvents(void[] buf) {
        auto len = handle.yieldRead!(unistd.read)(buf.ptr, buf.length);
        assert (len >= inotify.inotify_event.sizeof && len <= buf.length);
        return EventsRange(buf[0 .. len]);
    }
}

