module angstrom.reactor.lowlevel.fibril;

import core.thread: Thread;
import core.atomic;

import angstrom.lib.types: setPtrToInit, roundDown;
import angstrom.log;


version (D_InlineAsm_X86_64) version (Posix) {
    private pure nothrow @trusted @nogc:

    void* _fibril_init_stack(void* stackTop, void function(void*) nothrow fn, void* opaque) {
        // set rsp to top of stack, and make sure it's 16-byte aligned
        auto rsp = cast(void*)roundDown!16(cast(ulong)stackTop);
        auto rbp = rsp;

        void push(void* v) nothrow pure @nogc {
            rsp -= v.sizeof;
            *(cast(void**)rsp) = v;
        }

        push(null);                     // Fake RET of entrypoint
        push(&_fibril_trampoline);      // RIP
        push(rbp);                      // RBP
        push(null);                     // RBX
        push(null);                     // R12
        push(null);                     // R13
        push(fn);                       // R14
        push(opaque);                   // R15

        return rsp;
    }

    extern(C) void _fibril_trampoline() nothrow {
        pragma(inline, false);
        asm pure nothrow @nogc {
            naked;
            mov RDI, R14;  // fn
            mov RSI, R15;  // opaque

            // this has to be a jmp (not a call), otherwise exception-handling will see
            // this function in the stack and be... unhappy
            jmp _fibril_wrapper;
        }
    }

    extern(C) void _fibril_switch(void** fromRSP /* RDI */, void* toRSP /* RSI */) {
        pragma(inline, false);
        asm pure nothrow @nogc {
            naked;

            // save current state, then store RSP into `fromRSP`
            // RET is already pushed at TOS
            push RBP;
            push RBX;
            push R12;
            push R13;
            push R14;
            push R15;
            mov [RDI], RSP;

            // set RSP to `toRSP` and load state
            // and return to caller (RET is at TOS)
            mov RSP, RSI;
            pop R15;
            pop R14;
            pop R13;
            pop R12;
            pop RBX;
            pop RBP;
            ret;
        }
    }
}

extern(C) private void _fibril_wrapper(void function(void*) fn /* RDI */, void* opaque /* RSI */) nothrow {
    import core.stdc.stdlib: abort;
    import core.sys.posix.unistd: write;

    void writeErr(const(char[]) text) {
        write(2, text.ptr, text.length);     // Write error directly to stderr
    }

    try {
        fn(opaque);
        writeErr("Fibril function must never return\n");
    }
    catch (Throwable ex) {
        writeErr("Fibril function must never throw\n");
        try {ex.toString(&writeErr);} catch (Throwable) {}
        writeErr("\n");
    }
    // we abort twice so that there wouldn't be tail-call optimization and the function will remain on the stack
    // (useful for tracebacks)
    abort();
    abort();
    assert(false);
}


private {
    alias StackContext = getMemberType!(Thread, "m_main");
    pragma(mangle, "_D4core6thread6Thread7sm_mainCQBcQBaQw") extern __gshared Thread mainThread;

    extern(C) void* _d_eh_swapContext(void* newContext) nothrow @nogc;
    extern(C) void* _d_eh_swapContextDwarf(void* newContext) nothrow @nogc;

    pragma(mangle, "_D4core6thread6Thread3addFNbNiPSQBeQBcQy7ContextZv")
        void threadAddContext(StackContext*) nothrow @nogc;
    pragma(mangle, "_D4core6thread6Thread6removeFNbNiPSQBhQBfQBb7ContextZv")
        void threadRemoveContext(StackContext*) nothrow @nogc;

    private void* threadSwapContext(void* newEhConext) nothrow @nogc {
        if (auto p = _d_eh_swapContext(newEhConext)) {
            return p;
        }
        else if (auto p = _d_eh_swapContextDwarf(newEhConext)) {
            return p;
        }
        else {
            return null;
        }
    }
}

struct Fibril {
    private __gshared static StackContext* mainStackContext;
    private enum void* MAIN_FIBER = cast(void*)ulong.max;

    StackContext stackContext;
    // note: we abuse `stackContext.within`, it's not needed in our impl

    shared static this() {
        mainStackContext = &(mainThread.getMember!"m_curr");
    }

    static void initMainFibril(Fibril* fibril) {
        fibril.stackContext = StackContext.init;
        fibril.stackContext.within = MAIN_FIBER;
    }

    @property bool isMain() const pure nothrow @nogc @safe {pragma(inline, true);
        return stackContext.within == MAIN_FIBER;
    }
    @property void** rsp() nothrow @nogc {pragma(inline, true);
        return isMain ? &mainStackContext.tstack : &stackContext.tstack;
    }

    static Fibril* create(void[] stackArea) nothrow @nogc {
        auto fib = cast(Fibril*)(stackArea.ptr + (stackArea.length - this.sizeof));
        setPtrToInit(fib);

        fib.stackContext.bstack = stackArea.ptr + stackArea.length;
        fib.stackContext.tstack = fib.stackContext.bstack - this.sizeof;

        DEBUG!"fibril %s has stack [%s..%s)"(fib, stackArea.ptr, stackArea.ptr + stackArea.length);
        threadAddContext(fib.stackContext);
        return fib;
    }

    void destroy() nothrow @nogc {
        assert (!isMain);
        threadRemoveContext(&stackContext);
    }

    void reset() nothrow @nogc {
        assert (!isMain);
        fib.stackContext.tstack = fib.stackContext.bstack - this.sizeof;
        stackContext.ehContext = null;
    }

    void set(void function(Fibril*) fn, size_t reservedSize) nothrow @nogc {
        assert (!isMain);
        assert (fib.stackContext.tstack is fib.stackContext.bstack - this.sizeof);

        stackContext.tstack = _fibril_init_stack(stackContext.bstack - (this.sizeof + reservedSize),
            cast(void function(void*) nothrow)fn, &this);
        stackContext.ehContext = null;
    }

    void switchTo(Fibril* nextFib) nothrow @nogc {
        if (&this is nextFib) {
            return;
        }

        // note: we're taking a few shortcuts here since we rely on GC being disabled at all times
        // except when explictly enabled in the main fiber. also, we allow only the main thread to run fibers.
        // confer with `core.thread.Fiber.switchIn` for the full version

        stackContext.ehContext = threadSwapContext(nextFib.stackContext.ehContext);
        mainThread.getMember!"m_curr" = nextFib.stackContext;
        _fibril_switch(rsp, nextFib.rsp);
    }
}



