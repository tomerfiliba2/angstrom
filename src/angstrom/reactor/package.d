module angstrom.reactor;


struct Reactor {
__gshared static public:

    void open() {
    }
    void close() {
    }

/+
    //
    // core
    //
    public @property bool isActive() nothrow @nogc {
    public @property FiberHandle thisFiberHandle() nothrow @nogc {
    public void start() {
    public void stop() {
    public void enterCriticalSection() nothrow @nogc {
    public void leaveCriticalSection() nothrow @nogc {
    @property public bool isInCriticalSection() nothrow @nogc {
    void requestGarbageCollection() {
    public void killFiber(Fiber* fib) {
    public void throwInFiber(Fiber* fib, Throwable ex) {
    public Fiber* spawn(void delegate() dg, size_t reservedSize = 0) /*nothrow*/ @nogc {
    void _resumeFiber(FiberHandle fibHandle) nothrow @nogc {pragma(inline, true);
    public void _suspendThisFiber() @nogc {pragma(inline, true);

    public void yield() @nogc {
        DEBUG!"yielding %s"(thisFiber);
        _resumeFiber(thisFiber);
        _suspendThisFiber();
    }

    public void sleep(Duration dur) @nogc {
        DEBUG!"putting %s to sleep for %s"(thisFiber, dur);
        import angstrom.timers: TimerSubsystem;

        auto handle = TimerSubsystem.callIn!_resumeFiberHandle(dur, thisFiberHandle);
        scope(exit) handle.cancel();
        _suspendThisFiber();
    }

    //
    // timers
    //
    TimerHandle callNow(void delegate() dg) nothrow @nogc {
    TimerHandle callIn(Duration dur, void delegate() dg) nothrow @nogc {
    TimerHandle callAt(TscTimePoint timePoint, void delegate() dg) nothrow @nogc {
    TimerHandle callEvery(Duration dur, void delegate() dg) nothrow @nogc {

    //
    // threading
    //
    auto deferToThread(alias F)(Parameters!F args) @nogc
+/

    /+public auto timed(Duration dur) {
        static struct Timed {
            private FiberHandle fib;
            private TimerHandle timer;
            @disable this(this);
            ~this() {
                fib = null;
                timer.cancel();
            }
        }

        void blowUp() {
            //throwInFiber(fib, OperationTimedout);
            fib = null;
        }

        return Timed(thisFiberHandle, TimerSubsystem.callIn!_resumeFiberHandle(dur, &blowUp));
    }

    ...

    with(timed(10.seconds)) {
        myfile.read(buf);
    }

    +/


}
