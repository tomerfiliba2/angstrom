module angstrom.reactor.subproc;

import std.exception;
import std.string;
static import unistd = core.sys.posix.unistd;
static import waitmod = core.sys.posix.sys.wait;
static import sigmod = core.sys.posix.signal;
import core.sys.posix.sys.types: pid_t;
import angstrom.reactor.io;
import angstrom.reactor.sync: EdgeTriggeredEvent;


struct ProcessStatus {
    int value = -1;
    enum invalid = ProcessStatus(-1);

    @property bool isValid() const nothrow @nogc {return value != -1;}
    @property bool exited() const nothrow @nogc {assert(isValid); return waitmod.WIFEXITED(value);}
    @property bool signaled() const nothrow @nogc {assert(isValid); return waitmod.WIFSIGNALED(value);}
    @property int  exitStatus() const nothrow @nogc {assert (exited); return waitmod.WEXITSTATUS(value);}
    @property int  termSignal() const nothrow @nogc {assert (signaled); return waitmod.WTERMSIG(value);}
    @property int  retCode() const nothrow @nogc {return !isValid ? -1 : (exited ? exitStatus() : -termSignal());}
    @property bool success() const nothrow @nogc {return retCode() == 0;}
}

struct Process {
    private __gshared static bool _inited;
    private __gshared static EdgeTriggeredEvent gotSigChild;
    pid_t pid;
    ProcessStatus status;
    Pipe stdin;
    Pipe stdout;
    Pipe stderr;

    static Process spawn(string executable, string[] args, string[] env = null, void function() preExecFunc = null) {
        __gshared char[16*1024] argsBuf;
        __gshared char[16*1024] envBuf;
        char*[] argsVec;
        char*[] envVec;

        Pipe inRead, inWrite, outRead, outWrite, errRead, errWrite;
        Pipe.openPair(inRead, inWrite);
        Pipe.openPair(outRead, outWrite);
        Pipe.openPair(errRead, errWrite);

        pid_t pid = unistd.fork();
        errnoEnforce(pid >= 0, "fork");
        if (pid == 0) {
            // child
            inWrite.close();
            outRead.close();
            errRead.close();
            inRead.dupTo(0);
            outWrite.dupTo(1);
            errWrite.dupTo(2);
            inRead.close();
            outWrite.close();
            errWrite.close();

            FileHandle.iterOpenHandles((handle) {
                if (handle.getFD >= 3) {
                    handle.close();
                }
            });

            if (preExecFunc !is null) {
                preExecFunc();
            }
            unistd.execve(executable.toStringz(), argsVec.ptr, envVec.ptr);
            // execve returning means a failure...
            errnoEnforce(false, "execve");
            assert(false);
        }
        else {
            // parent
            inRead.close();
            outWrite.close();
            errWrite.close();
            return Process(pid, ProcessStatus.invalid, inWrite, outRead, errRead);
        }
    }

    static int run(string[] executableAndArgs, ref char[] outBuf, ref char[] errBuf, string[] env = null) {
        return -1;
    }
    static int run(string[] executableAndArgs, ref char[] outBuf, string[] env = null) {
        return -1;
    }
    static char[] checkedRun(string[] executableAndArgs, char[] outBuf, string[] env = null) {
        return null;
    }

    bool poll() {
        if (pid < 0) {
            return true;
        }
        auto res = waitmod.waitpid(pid, &status.value, waitmod.WNOHANG);
        errnoEnforce(res >= 0, "waitpid");
        if (res == pid) {
            pid = -1;
            return true;
        }
        return false;
    }

    void wait() {
        if (!_inited) {
            SignalHandler.register!"SIGCHLD"((siginfo) {
                // we don't expect to have hundreds of running processes, so waking all waiters up seems legit.
                // if there'd be many, a global hash table that indexes every child proc by pid (siginfo.si_pid) is needed
                gotSigChild.signal();
            });
            _inited = true;
        }

        while (!poll()) {
            gotSigChild.wait();
        }
    }

    void sendSignal(int signum) {
        assert(pid > 0);
        errnoEnforce(sigmod.kill(pid, signum) == 0);
    }
    void sendSignal(string signame)() {
        static assert (signame.startsWith("SIG"));
        sendSignal(__traits(getMember, sigmod, signame));
    }
    void terminate() {
        sendSignal!"SIGTERM"();
    }
    void kill() {
        sendSignal!"SIGKILL"();
    }
}


