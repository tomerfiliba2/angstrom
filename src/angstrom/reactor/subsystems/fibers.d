module angstrom.reactor.subsystems.fibers;

import core.memory: GC;
import std.exception: errnoEnforce;
import std.datetime: Duration;

import angstrom.lib;
import angstrom.logging;
import angstrom.reactor.lowlevel.fibril;


class FiberInterrupt: Throwable {
    this() {super("FiberInterrupt");}
    this(string msg) {super(msg);}
}
class FiberKill: FiberInterrupt {
    this() {super("FiberKill");}
}
class ReactorStopped: Throwable {
    this() {super("ReactorStopped");}
}

package struct Fiber {
    enum Owner: ubyte {
        NONE,
        FREE_LIST,
        SCHEDULED_LIST,
    }

    enum Flags: ushort {
        STARTED      = 0x0001,
        PRIORITIZED  = 0x0002,
        DEFUNCT      = 0x0004,
        //SPAWNED    = 0x0008
    }

    Owner  owner;
    ushort _flags;
    int globalIndex;
    uint loggerId;
    Fiber* next;
    Fiber* prev;
    Fiber* returnTo;
    Throwable storedEx;
    void delegate() dg;

    @property bool flag(string name)() const pure nothrow @nogc {
        return (_flags & __traits(getMember, Flags, name)) != 0;
    }
    @property void flag(string name)(bool val) pure nothrow @nogc {
        if (val) {
            _flags |= __traits(getMember, Flags, name);
        }
        else {
            _flags &= cast(ushort)(~cast(uint)__traits(getMember, Flags, name));
        }
    }

    @property Fibril* fibril() nothrow @nogc {
        return cast(Fibril*)((cast(void*)&this) + this.sizeof);
    }

    static Fiber* fromFibril(Fibril* fibril) nothrow @nogc {
        return cast(Fiber*)((cast(void*)fibril) - Fiber.sizeof);
    }

    static Fiber* create(void[] stackArea) nothrow @nogc {
        Fibril* fibril = Fibril.create(stackArea);
        Fiber* fiber = fromFibril(fibril);
        *fiber = Fiber.init;
        DEBUG!"created fiber %s (fibril %s)"(fiber, fibril);
        fiber.reset();
        __gshared static uint loggerIdCounter = 1;
        fiber.globalIndex = -1;
        fiber.loggerId = loggerIdCounter++;
        return fiber;
    }
    void destroy() nothrow @nogc {
        DEBUG!"destroying fiber %s (fibril %s)"(&this, fibril);
        fibril.destroy();
    }

    void reset() nothrow @nogc {
        owner = Owner.NONE;
        _flags = 0;
        next = null;
        storedEx = null;
        dg = null;
        returnTo = null;
        this.loggerId += 4096;
        fibril.reset();
    }

    void set(void function(Fibril*) fn, size_t reservedSize) nothrow @nogc {
        assert (owner == Owner.NONE);
        DEBUG!"setting wrapper %s for %s"(fn, &this);
        fibril.set(fn, this.sizeof + reservedSize);
    }
}

public struct FiberHandle {
    private int fibIdx = -1;
    private uint incarnation = uint.max;
    static assert (this.sizeof == (void*).sizeof);

    enum invalid = FiberHandle.init;

    this(Fiber* fib) nothrow @nogc {
        opAssign(fib);
    }
    ref FiberHandle opAssign(Fiber* fib) nothrow @nogc {
        if (fib is null) {
            fibIdx = -1;
        }
        else {
            fibIdx = fib.globalIndex;
            incarnation = fib.loggerId;
        }
        return this;
    }
    ref FiberHandle opAssign(FiberHandle fh) nothrow @nogc {
        fibIdx = fh.fibIdx;
        incarnation = fh.incarnation;
        return this;
    }

    @property bool isValid() const nothrow @nogc {
        return fibIdx < 0 ? false : (FiberScheduler.fibers[fibIdx].loggerId == incarnation);
    }

    Fiber* get() nothrow @nogc {
        if (fibIdx < 0) {
            return null;
        }
        Fiber* fib = FiberScheduler.fibers[fibIdx];
        return fib.loggerId == incarnation ? fib : null;
    }
}

private align(1) struct MainFiberHolder {
align(1):
    ubyte[128] _reserved;
    Fiber  fiber;
    Fibril fibril;

    void initMain() {
        Fibril.initMainFibril(&fibril);
    }
}

struct FiberScheduler {
__gshared static private:
    MainFiberHolder _mainFiberHolder;
    Fiber* thisFiber;
    Fiber* idleFiber;
    // XXX these can be made flags
    bool active;
    bool shouldCollectGC;
    int criticalSectionLevel;
    IntrusiveList!Fiber freeFibersList;
    IntrusiveList!Fiber scheduledFibersList;
    Fiber*[] fibers;
    void[] stackArea;

    public void open(size_t numFibers, size_t fiberStackSize) {
        import core.sys.posix.sys.mman;
        import core.sys.linux.sys.mman: MAP_ANONYMOUS, MAP_POPULATE;

        DEBUG!"FiberScheduler opening, numFibers=%s+2"(numFibers);

        // '0' is reserved for mainFiber, '1' is reserved for idleFiber, so add two more
        numFibers += 2;

        auto alignedStackSize = ((fiberStackSize + SYS_PAGE_SIZE - 1) / SYS_PAGE_SIZE) * SYS_PAGE_SIZE + SYS_PAGE_SIZE;
        auto totalStackSize = alignedStackSize * (numFibers - 1);
        auto basePtr = mmap(null, totalStackSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        assert (basePtr != MAP_FAILED);
        stackArea = basePtr[0 .. totalStackSize];

        _mainFiberHolder.initMain();
        thisFiber = mainFiber;
        mainFiber.globalIndex = 0;

        fibers.length = numFibers;
        foreach(int i, ref fib; fibers) {
            if (i == 0) {
                fib = mainFiber;
                continue;
            }
            errnoEnforce(mprotect(basePtr + (i-1) * alignedStackSize, SYS_PAGE_SIZE, PROT_NONE) == 0, "mprotect");
            fib = Fiber.create(basePtr[(i-1) * alignedStackSize + SYS_PAGE_SIZE .. i * alignedStackSize]);
            fib.globalIndex = i;
            if (i == 1) {
                idleFiber = fib;
            }
            else {
                _addToFreeList(fib);
            }
        }
    }

    public void close() {
        import core.sys.posix.sys.mman;

        DEBUG!"FiberScheduler closing"();
        foreach(ref fib; fibers) {
            if (fib.owner != Fiber.Owner.FREE_LIST && fib.flag!"STARTED") {
                killFiber(fib);
            }
        }
        freeFibersList.removeAll();
        scheduledFibersList.removeAll();
        foreach(ref fib; fibers) {
            fib.destroy();
        }
        fibers.length = 0;
        idleFiber = null;
        thisFiber = mainFiber;
        errnoEnforce(munmap(stackArea.ptr, stackArea.length) == 0, "munmap");
    }

    public @property bool isActive() nothrow @nogc {
        return active;
    }

    public @property FiberHandle thisFiberHandle() nothrow @nogc {
        return FiberHandle(thisFiber);
    }
    public @property Fiber* thisFiberPtr() nothrow @nogc {
        return thisFiber;
    }

    @property Fiber* mainFiber() nothrow @nogc {
        return &_mainFiberHolder.fiber;
    }

    public void start() {
        assert (!active);
        active = true;

        GC.collect();
        GC.disable();
        scope(exit) GC.enable();

        try {
            while (active) {
                DEBUG!"mainloop"();
                _switchToNext();
                if (shouldCollectGC) {
                    shouldCollectGC = false;
                    _collectGC();
                }
                /+if (scheduledFibersList.empty) {
                    _switchToFiber(idleFiber);
                }+/
            }
        }
        catch (ReactorStopped) {
            DEBUG!"ReactorStopped"();
        }
    }

    public void stop() {
        DEBUG!"stopping"();
        if (active) {
            active = false;
            __gshared static stoppedEx = new ReactorStopped();
            _throwInMain(stoppedEx);
        }
    }

    public void enterCriticalSection() nothrow @nogc {
        assert (criticalSectionLevel >= 0);
        criticalSectionLevel++;
    }
    public void leaveCriticalSection() nothrow @nogc {
        assert (criticalSectionLevel > 0);
        criticalSectionLevel--;
    }
    @property public bool isInCriticalSection() nothrow @nogc {
        return criticalSectionLevel > 0;
    }

    void _collectGC() {
        GC.enable();
        scope(exit) GC.disable();
        GC.collect();
    }

    public void killFiber(Fiber* fib) {
        assert (fib.owner != Fiber.Owner.FREE_LIST);
        INFO!"killFiber(%s)"(fib);
        enterCriticalSection();
        scope(exit) leaveCriticalSection();
        if (fib.owner == Fiber.Owner.SCHEDULED_LIST) {
            scheduledFibersList.remove(fib);
            fib.owner = Fiber.Owner.NONE;
        }
        fib.storedEx = new FiberKill();
        fib.returnTo = thisFiber;
        _switchToFiber(fib);
    }

    public void throwInFiber(Fiber* fib, Throwable ex) {
        INFO!"throwInFiber %s in %s"(cast(void*)ex, fib);
        if (fib is thisFiber) {
            throw ex;
        }
        else {
            fib.storedEx = ex;
            _resumeFiber(fib);
        }
    }

    void _switchToFiber(Fiber* nextFib) @nogc {
        DEBUG!"switch from %s to %s"(thisFiber, nextFib);

        __gshared static Fiber* prevFiber;
        prevFiber = thisFiber;
        thisFiber = nextFib;
        prevFiber.fibril.switchTo(nextFib.fibril);
        // ---- this is running the context of nextFib now ----

        loggerId = thisFiber.loggerId;

        if (prevFiber.flag!"DEFUNCT") {
            _addToFreeList(prevFiber);
        }
        prevFiber = null;

        if (thisFiber.storedEx) {
            auto ex = thisFiber.storedEx;
            thisFiber.storedEx = null;
            throw ex;
        }
    }

    void _switchToMain() @nogc {
        _switchToFiber(mainFiber);
    }

    void _throwInMain(Throwable ex) {
        if (thisFiber is mainFiber) {
            throw ex;
        }
        mainFiber.storedEx = ex;
        _switchToMain();
    }

    void _switchToNext() @nogc {
        assert (criticalSectionLevel == 0, "cannot switch inside critical section");
        Fiber* next;
        if (scheduledFibersList.empty) {
            next = mainFiber;
        }
        else {
            next = scheduledFibersList.popHead();
            assert (next.owner == Fiber.Owner.SCHEDULED_LIST);
            next.owner = Fiber.Owner.NONE;
        }
        _switchToFiber(next);
    }

    void _addToFreeList(Fiber* fib) nothrow @nogc {
        DEBUG!"returning %s to free list; owner=%s"(fib, fib.owner);
        assert (fib.owner == Fiber.Owner.NONE);
        fib.reset();
        fib.owner = Fiber.Owner.FREE_LIST;
        freeFibersList.prepend(fib);
    }

    Fiber* _allocFiber() nothrow @nogc {
        assert (!freeFibersList.empty);
        auto fib = freeFibersList.popHead();
        DEBUG!"fetched %s"(fib);
        assert (fib.owner == Fiber.Owner.FREE_LIST);
        fib.owner = Fiber.Owner.NONE;
        return fib;
    }

    public Fiber* spawn(void delegate() dg, size_t reservedSize = 0) /*nothrow*/ @nogc {
        auto fib = _allocFiber();
        fib.dg = dg;
        DEBUG!"spawning %s in %s"(dg.funcptr, fib);
        fib.set(&_wrapper, reservedSize);
        _resumeFiber(fib);
        return fib;
    }

    void _wrapper(Fibril* fibril) {
        import core.stdc.stdlib: abort;

        Fiber* fib = Fiber.fromFibril(fibril);
        thisFiber = fib;
        loggerId = fib.loggerId;
        DEBUG!"wrapper(%s)"(fib);

        fib.flag!"STARTED" = true;
        try {
            fib.dg();
        }
        catch (FiberInterrupt ex) {
            // treated like normal termination
            INFO!"FiberInterrupt %s"(typeid(ex).name);
        }
        catch (Throwable ex) {
            ERROR!"wrapper(%s) caught %s(%s) at %s:%s"(fib, typeid(ex).name, ex.msg, ex.file, ex.line);
            fib.flag!"DEFUNCT" = true;
            fib.flag!"STARTED" = false;
            _throwInMain(ex);
            abort();
            assert(false);
        }
        DEBUG!"wrapper(%s) done"(fib);
        fib.flag!"DEFUNCT" = true;
        fib.flag!"STARTED" = false;
        if (fib.returnTo is null) {
            _switchToNext();
        }
        else {
            auto dest = fib.returnTo;
            fib.returnTo = null;
            _switchToFiber(dest);
        }
        abort();
        assert(false);
    }

    public void _resumeFiber(Fiber* fib) nothrow @nogc {
        if (fib.owner == Fiber.Owner.SCHEDULED_LIST) {
            return;
        }
        assert (fib.owner == Fiber.Owner.NONE);
        //assert (fib.flag!"SPAWNED");
        fib.owner = Fiber.Owner.SCHEDULED_LIST;
        DEBUG!"scheduling %s"(fib);
        if (fib.flag!"PRIORITIZED") {
            fib.flag!"PRIORITIZED" = false;
            scheduledFibersList.prepend(fib);
        }
        else {
            scheduledFibersList.append(fib);
        }
    }

    public void _resumeFiber(FiberHandle fibHandle) nothrow @nogc {pragma(inline, true);
        if (auto fib = fibHandle.get()) {
            _resumeFiber(fib);
        }
    }

    public void _suspendThisFiber() @nogc {pragma(inline, true);
        assert (thisFiber !is mainFiber);
        _switchToNext();
    }
}


