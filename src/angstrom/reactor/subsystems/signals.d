module angstrom.reactor.subsystems.signals;

import angstrom.reactor.io;


struct SignalHandler {
__gshared static:
    enum MAX_NUM_SIGNALS = 64;
    private FileHandle sigFD;
    private signalmod.sigset_t sigset;
    private void delegate(const(signalfd.signalfd_siginfo)*)[MAX_NUM_SIGNALS] handlers;

    private void _open() {
        signalmod.sigemptyset(&sigset);
        int fd = signalfd.signalfd(-1, &sigset, 0);
        errnoEnforce(fd >= 0, "signalfd");
        sigFD = FileHandle(fd);
        Epoller.registerRead(sigFD);
        sigFD.getEntry.setHandler(&readSignals, null);

        __gshared static ubyte[8*4096] alternateStack;
        signalmod.stack_t stackInfo = {
            ss_sp: alternateStack.ptr,
            ss_size: alternateStack.sizeof,
        };
        errnoEnforce(signalmod.sigaltstack(&stackInfo, null) == 0, "sigstack");
    }
    void close() {
        signalmod.sigemptyset(&sigset);
        errnoEnforce(signalmod.sigprocmask(signalmod.SIG_SETMASK, &sigset, null), "sigprocmask");
        sigFD.close();
    }
    @property bool closed() nothrow @nogc {
        return sigFD.closed;
    }

    void register(int signum, void delegate(const(signalfd.signalfd_siginfo)*) dg) {
        assert (signum >= 0 && signum < MAX_NUM_SIGNALS);
        if (handlers[signum] is dg) {
            return;
        }
        assert (handlers[signum] is null);

        if (sigFD.closed) {
            _open();
        }

        signalmod.sigaddset(&sigset, signum);
        errnoEnforce(signalmod.sigprocmask(signalmod.SIG_SETMASK, &sigset, null), "sigprocmask");
        sigFD.checkedCall!(signalfd.signalfd)(&sigset, 0);
        handlers[signum] = dg;
    }
    void register(string signame)(void delegate(const(signalfd.signalfd_siginfo)*) dg) {
        register(__traits(getMember, signalmod, signame), dg);
    }

    void unregister(int signum) {
        assert (signum >= 0 && signum < MAX_NUM_SIGNALS);
        if (sigFD.closed) {
            return;
        }

        signalmod.sigdelset(&sigset, signum);
        errnoEnforce(signalmod.sigprocmask(signalmod.SIG_SETMASK, &sigset, null), "sigprocmask");
        sigFD.checkedCall!(signalfd.signalfd)(&sigset, 0);
    }
    void unregister(string signame)() {
        unregister(__traits(getMember, signalmod, signame));
    }

    private void readSignals(FileEntry*) {
        signalfd.signalfd_siginfo[16] infos = void;
        while (true) {
            auto len = sigFD.call!(unistd.read)(infos.ptr, infos.sizeof);
            if (len == 0) {
                break;
            }
            else if (len < 0) {
                if (errnomod.errno == errnomod.EINTR) {
                    continue;
                }
                errnoEnforce(errnomod.errno == errnomod.EAGAIN, "read");
                break;
            }

            assert (len % signalfd.signalfd_siginfo.sizeof == 0);
            foreach(ref info; infos[0 .. len / signalfd.signalfd_siginfo.sizeof]) {
                if (auto dg = handlers[info.ssi_signo]) {
                    dg(&info);
                }
            }
        }
    }

    alias extern(C) void function(int, signalmod.siginfo_t*, void*) sigaction_func;

    void registerImmediate(int signum, sigaction_func fn) {
        assert(!signalmod.sigismember(&sigset, signum), "cannot be registered as both");

        signalmod.sigaction_t action = {
            sa_sigaction: fn,
            sa_flags: signalmod.SA_SIGINFO | signalmod.SA_ONSTACK,
        };
        errnoEnforce(signalmod.sigaction(signum, &action, null) == 0, "sigaction");
    }
}

