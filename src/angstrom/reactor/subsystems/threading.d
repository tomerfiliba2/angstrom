module angstrom.reactor.subsystems.threading;

import core.thread: Thread;
import std.datetime: usecs;
import std.exception;
static import signalmod = core.sys.posix.signal;
static import pthread = core.sys.posix.pthread;
import angstrom.containers.pools: Pool;
import angstrom.arch.concurrent_queue: ConcurrentQueue;
import angstrom.arch.linux: gettid;
import angstrom.timers: TimerHandle, TimerSubsystem;
import angstrom.reactor: Reactor, FiberHandle;


class BGThread: Thread {
    // background threads are not meant to handle any signals...
    // leave it for the main thread
    __gshared static immutable BLOCKED_SIGNALS = [
        signalmod.SIGHUP, signalmod.SIGINT, signalmod.SIGQUIT,
        //signalmod.SIGILL, signalmod.SIGTRAP, signalmod.SIGABRT,
        //signalmod.SIGBUS, signalmod.SIGFPE, signalmod.SIGKILL,
        //signalmod.SIGUSR1, signalmod.SIGSEGV, signalmod.SIGUSR2,
        signalmod.SIGPIPE, signalmod.SIGALRM, signalmod.SIGTERM,
        //signalmod.SIGSTKFLT, signalmod.SIGCONT, signalmod.SIGSTOP,
        signalmod.SIGCHLD, signalmod.SIGTSTP, signalmod.SIGTTIN,
        signalmod.SIGTTOU, signalmod.SIGURG, signalmod.SIGXCPU,
        signalmod.SIGXFSZ, signalmod.SIGVTALRM, signalmod.SIGPROF,
        /+signalmod.SIGWINCH,+/ signalmod.SIGPOLL, /+signalmod.SIGPWR,+/
        //signalmod.SIGSYS,
    ];

    //__gshared static void delegate(WorkerThread) preThreadFunc;

    align(8) int kernel_tid = -1;
    void delegate() dg;

    this(void delegate() dg, size_t stackSize = 0) {
        kernel_tid = -1;
        this.dg = dg;
        this.isDaemon = true;
        super(&wrapper, stackSize);
    }

    private void wrapper() {
        scope(exit) kernel_tid = -1;
        kernel_tid = gettid();

        signalmod.sigset_t sigset = void;
        assert(signalmod.sigemptyset(&sigset) == 0, "sigemptyset failed");
        foreach(sig; BLOCKED_SIGNALS) {
            assert(signalmod.sigaddset(&sigset, sig) == 0, "sigaddset failed");
        }
        foreach(sig; signalmod.SIGRTMIN .. signalmod.SIGRTMAX /* +1? */) {
            assert(signalmod.sigaddset(&sigset, sig) == 0, "sigaddset failed");
        }
        errnoEnforce(pthread.pthread_sigmask(signalmod.SIG_BLOCK, &sigset, null) == 0, "pthread_sigmask");

        try {
            //if (preThreadFunc) {
            //    // set sched priority, move to CPU set
            //    preThreadFunc(this);
            //}
            dg();
        }
        catch (Throwable ex) {
            try{import std.stdio; writeln(ex);} catch(Throwable){}
            assert(false);
        }
    }
}

private struct PthreadMutex {
    pthread.pthread_mutex_t mtx;

    void lock() nothrow @nogc {
        auto res = pthread.pthread_mutex_lock(&mtx);
        assert(res == 0, "pthread_mutex_lock");
    }
    void unlock() nothrow @nogc {
        auto res = pthread.pthread_mutex_unlock(&mtx);
        assert(res == 0, "thread_mutex_unlock");
    }
}


struct ThreadPool {
__gshared static:
    struct Task {
        enum State: ulong {
            PENDING,
            THREAD_EXECUTING,
            THREAD_FINISHED,
            DONE,
        }
        FiberHandle fib;
        State state;
        Throwable ex;
        void delegate() dg;
    }

    private enum MAX_FETCH_BATCH_SIZE = 32;
    private ConcurrentQueue!(Task*) requestsQueue;
    private ConcurrentQueue!(Task*) responsesQueue;
    Pool!Task tasksPool;
    PthreadMutex pollerMutex;
    private BGThread[] threads;
    TimerHandle fetcherHandle;

    void open(int numThreads, int numTasks = 4096) {
        //if (numThreads > threads.lengt)
        tasksPool.open(numTasks);
        requestsQueue.reset(numTasks);
        responsesQueue.reset(numTasks);

        void responseFetcher() {
            foreach(_; 0 .. MAX_FETCH_BATCH_SIZE) {
                Task* t = cast(Task*)responsesQueue.pop();
                if (t is null) {
                    break;
                }
                assert (t.state == Task.State.THREAD_FINISHED);
                t.state = Task.State.DONE;
                if (t.fib.isValid) {
                    Reactor._resumeFiber(t.fib);
                }
                else {
                    tasksPool.release(t);
                }
            }
        }

        fetcherHandle = TimerSubsystem.callEvery(100.usecs, &responseFetcher);
    }
    void close() {
        tasksPool.close();
        fetcherHandle.cancel();
    }

    private void threadFunc() {
        while (true) {
            Task* t = cast(Task*)requestsQueue.pop();
            if (t is null) {
                continue;
            }
            assert (t.state == Task.State.PENDING);
            t.state = Task.State.THREAD_EXECUTING;
            try {
                t.dg();
            }
            catch (Throwable ex) {
                t.ex = ex;
            }
            t.state = Task.State.THREAD_FINISHED;
            responsesQueue.push(t);
        }
    }

    auto yieldTo(void delegate() dg) {
        auto task = tasksPool.alloc();
        task.state = Task.State.PENDING;
        task.ex = null;
        task.dg = dg;
        task.fib = Reactor.thisFiberHandle;
        requestsQueue.push(task);
        try {
            Reactor._suspendThisFiber();
        }
        catch (Throwable ex) {
            // XXX make sure this is atomic
            task.fib = FiberHandle.invalid;
            throw ex;
        }

        assert (task.state == Task.State.DONE);
        tasksPool.release(task);
    }


}


