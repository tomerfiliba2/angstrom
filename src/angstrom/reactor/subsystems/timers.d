module angstrom.timers;

import std.datetime: Duration, usecs;
import std.traits: Parameters;

import angstrom.lib: TscTimePoint;
import angstrom.reactor.lowlevel.timer_queue;
import angstrom.containers.pools: Pool;


private struct Timer {
    uint generation = TimerHandle.invalidGen;
    uint refcount;
    ulong timePoint;
    long interval;
    Timer* next;
    Timer* prev;
    void delegate() dg;
}

struct TimerHandle {
    enum uint invalidIdx = TimerSubsystem.timersPool.invalidIdx;
    enum uint invalidGen = uint.max;

    uint elemIdx = invalidIdx;
    uint generation = invalidGen;
    enum invalid = TimerHandle.init;

    private this(Timer* t) nothrow @nogc {
        elemIdx = TimerSubsystem.timersPool.indexOf(t);
        generation = t.generation;
        assert (generation != invalidGen);
    }

    this(this) nothrow @nogc {
        if (auto t = get()) {
            t.refcount++;
        }
    }
    ~this() nothrow @nogc {
        if (auto t = get()) {
            if (t.refcount == 1) {
                TimerSubsystem.cancel(t);
            }
            else {
                t.refcount--;
            }
        }
    }
    auto ref opAssign(typeof(null) th) nothrow @nogc {
        elemIdx = invalidIdx;
        generation = invalidGen;
        return this;
    }
    auto ref opAssign(TimerHandle th) nothrow @nogc {
        if (auto t = th.get()) {
            t.refcount++;
            elemIdx = th.elemIdx;
            generation = th.generation;
        }
        else {
            elemIdx = invalidIdx;
            generation = invalidGen;
        }
        return this;
    }

    @property private Timer* get() nothrow @nogc {
        if (elemIdx == invalidIdx || generation == invalidGen) {
            return null;
        }
        auto t = TimerSubsystem.timersPool.fromIndex(elemIdx);
        return t.generation == generation ? t : null;
    }
    @property bool isValid() nothrow @nogc {
        return get() !is null;
    }

    void cancel() nothrow @nogc {
        if (auto t = get()) {
            TimerSubsystem.cancel(t);
        }
        elemIdx = invalidIdx;
        generation = invalidGen;
    }
}

struct TimerSubsystem {
__gshared static:
    Pool!Timer timersPool;
    LogarithmicTimerQueue!Timer queue;

    void open(uint maxTimers) {
        timersPool.open(maxTimers);
        queue.open(TscTimePoint.now.cycles, TscTimePoint.cyclesPerSecond, 50.usecs);
    }
    void close() {
        timersPool.close();
    }

    private Timer* alloc(TscTimePoint timePoint, long interval) nothrow @nogc {
        __gshared static uint generationCounter = 2; // must be even, since we don't want it to ever reach uint.max
        assert (timePoint.cycles >= 0);

        auto t = timersPool.alloc();
        t.refcount = 1;
        t.timePoint = timePoint.cycles;
        t.interval = interval;
        t.generation = generationCounter;
        generationCounter += 2; // must inc by an even amount
        queue.insert(t);
        return t;
    }

    private void cancel(Timer* t) nothrow @nogc {
        t.generation = TimerHandle.invalidIdx;
        TimerSubsystem.queue.remove(t);
        TimerSubsystem.timersPool.release(t);
    }

    package bool peek(TscTimePoint now) {
        return queue.peek(now.cycles);
    }
    package void popReady(TscTimePoint now) {
        while (true) {
            auto t = queue.pop(now.cycles);
            if (t is null) {
                break;
            }
            if (t.interval == 0) {
                // regular timer
                t.dg();
                timersPool.release(t);
            }
            else {
                // recurring timer
                t.dg();
                t.timePoint = now.cycles + t.interval;
                queue.insert(t);
            }
        }
    }

    TimerHandle callNow(void delegate() dg) nothrow @nogc {
        auto t = alloc(TscTimePoint.zero, 0);
        return TimerHandle(t);
    }
    TimerHandle callIn(Duration dur, void delegate() dg) nothrow @nogc {
        //return callAt(TscTimePoint.now + dur, dg);
        return TimerHandle.invalid;
    }
    TimerHandle callIn(alias F)(Duration dur, Parameters!F args) nothrow @nogc {
        return TimerHandle.invalid;
    }

    TimerHandle callAt(TscTimePoint timePoint, void delegate() dg) nothrow @nogc {
        auto t = alloc(timePoint, 0);
        return TimerHandle(t);
    }
    TimerHandle callEvery(Duration dur, void delegate() dg) nothrow @nogc {
        auto t = alloc(TscTimePoint.now, TscTimePoint.toCycles(dur));
        return TimerHandle(t);
    }
}

