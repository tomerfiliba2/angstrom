module angstrom.reactor.sync;

import angstrom.lib;
import angstrom.logging;
import angstrom.reactor.subsystems.fibers: FiberScheduler, FiberHandle;


struct FiberQueue {
    static struct QueueMember {
        QueueMember* next;
        QueueMember* prev;
        FiberHandle fibHandle;
        void* opaque;
    }
    IntrusiveList!QueueMember list;

    @disable this(this);

    ~this() {
        // XXX: go over list and kill all waiters?
        assert (empty);
    }

    @property bool empty() const nothrow @nogc {
        return list.empty;
    }

    void suspendCaller(void* opaque = null) @nogc {
        QueueMember member;
        member.fibHandle = FiberScheduler.thisFiberHandle();
        member.opaque = opaque;
        list.append(&member);
        scope(exit) list.remove(&member);
        FiberScheduler._suspendThisFiber();
    }

    FiberHandle resumeOne() nothrow @nogc {
        if (empty) {
            return FiberHandle.invalid;
        }
        else {
            QueueMember* member = list.popHead();
            FiberScheduler._resumeFiber(member.fibHandle);
            return member.fibHandle;
        }
    }

    void resumeAll() nothrow @nogc {
        while (!empty) {
            resumeOne();
        }
    }

    void* peekOpaque() nothrow @nogc {
        assert (!empty);
        return list.head.opaque;
    }
}

struct EdgeTriggeredEvent {
    FiberQueue waiters;

    void wait() @nogc {
        waiters.suspendCaller();
    }
    void signal() nothrow @nogc {
        waiters.resumeAll();
    }
}

struct LevelEvent {
    private FiberQueue waiters;
    private bool _isSet;

    void set() nothrow @nogc {
        _isSet = true;
        waiters.resumeAll();
    }
    void reset() nothrow @nogc {
        _isSet = false;
    }
    void wait() @nogc {
        assert (!FiberScheduler.isInCriticalSection);
        if (!_isSet) {
            waiters.suspendCaller();
        }
    }
}

//
// Strict-FIFO semaphore
//
struct Semaphore {
    FiberQueue waiters;
    ulong balance;

    this(ulong capacity) nothrow @nogc {
        reset(capacity);
    }
    void reset(ulong capacity) nothrow @nogc {
        this.balance = 0;
        if (capacity > 0) {
            release(capacity);
        }
    }

    bool tryAcquire(ulong count = 1) nothrow @nogc {
        assert (count > 0);
        if (balance >= count && waiters.empty) {
            balance -= count;
            return true;
        }
        return false;
    }

    void acquire(ulong count = 1) @nogc {
        assert (!FiberScheduler.isInCriticalSection);
        if (tryAcquire(count)) {
            return;
        }

        ulong requested = count;

        scope (failure) {
            // release() will zero out the `requested` parameter if it woke us up. the following `if` handles the case
            // where we've been killed, still hadn't been switched-in (so we're still in the FiberQueue), and release()
            // also wished to wake us up, consuming budget for us. in this case, return the wrongfully-consumed budget
            if (requested == 0) {
                release(count);
            }
        }

        waiters.suspendCaller(&requested);
        assert (requested == 0);
    }

    void release(ulong count = 1) nothrow @nogc {
        assert (count > 0);
        balance += count;
        while (!waiters.empty) {
            ulong* requested = cast(ulong*)waiters.peekOpaque();
            if (balance < *requested) {
                break;
            }

            balance -= *requested;
            *requested = 0;
            waiters.resumeOne();
        }
    }

    static struct Acquisition {
        private Semaphore* sem;
        private ulong count;
        @disable this(this);
        ~this() nothrow @nogc {
            if (sem !is null) {
                sem.release(count);
                sem = null;
            }
        }
    }

    Acquisition acquisition(ulong count = 1) @nogc {
        acquire(count);
        return Acquisition(&this, count);
    }
}

//
// blocks "master" fiber until all "worker" fibers are done
//
struct Barrier {
    Semaphore sem = Semaphore(0);

    void markDone() nothrow @nogc {
        sem.release(1);
    }
    void waitFor(ulong count) @nogc {
        sem.acquire(count);
    }
}

//
// Strict-FIFO, non-reentrant lock
//
struct Lock {
    private FiberQueue waiters;
    private FiberHandle owner;

    @property bool isOwned() const nothrow @nogc {
        return owner.isValid() || !waiters.empty();
    }
    @property FiberHandle getOwner() const pure nothrow @nogc {
        return owner;
    }

    bool tryAcquire() nothrow @nogc {
        if (!owner.isValid() && waiters.empty()) {
            owner = FiberScheduler.thisFiberHandle();
            return true;
        }
        return false;
    }

    void acquire() @nogc {
        assert (!FiberScheduler.isInCriticalSection);
        if (tryAcquire()) {
            return;
        }

        scope(failure) {
            if (owner == FiberScheduler.thisFiberHandle) {
                release();
            }
        }
        waiters.suspendCaller();
        assert (owner == FiberScheduler.thisFiberHandle());
    }
    void release() nothrow @nogc {
        assert (owner == FiberScheduler.thisFiberHandle(), "Only owner can release");
        owner = waiters.resumeOne();  // will be null if no waiters left
    }

    static struct Acquisition {
        private Lock* lock;
        @disable this(this);
        ~this() nothrow @nogc {
            if (lock !is null) {
                lock.release();
            }
        }
    }

    Acquisition acquisition() @nogc {
        acquire();
        return Acquisition(&this);
    }
}

//
// Strict-FIFO shared/exclusive ("read/write") lock
//
struct RWLock {
    static struct Ownership {
        bool exclusive;
        bool granted;
    }

    FiberQueue waiters;
    size_t sharedOwners;
    FiberHandle exclusiveOwner;

    bool tryAcquireShared() nothrow @nogc {
        if (!exclusiveOwner.isValid() && waiters.empty()) {
            sharedOwners++;
            return true;
        }
        return false;
    }
    void acquireShared() @nogc {
        assert (!FiberScheduler.isInCriticalSection);
        if (tryAcquireShared()) {
            return;
        }

        auto ownership = Ownership(false, false);
        scope(failure) {
            if (ownership.granted) {
                releaseShared();
            }
        }
        waiters.suspendCaller(&ownership);
        assert (ownership.granted);
    }
    void releaseShared() nothrow @nogc {
        assert (sharedOwners > 0, "released too many times");
        assert (!exclusiveOwner.isValid());
        sharedOwners--;
        if (sharedOwners == 0) {
            _release();
        }
    }

    bool tryAcquireExclusive() @nogc {
        if (!exclusiveOwner.isValid && waiters.empty && sharedOwners == 0) {
            exclusiveOwner = FiberScheduler.thisFiberHandle();
            return true;
        }
        return false;
    }

    void acquireExclusive() @nogc {
        assert (!FiberScheduler.isInCriticalSection);
        if (tryAcquireExclusive()) {
            return;
        }

        auto ownership = Ownership(false, false);
        scope(failure) {
            if (exclusiveOwner == FiberScheduler.thisFiberHandle()) {
                releaseExclusive();
            }
        }

        waiters.suspendCaller(&ownership);
        assert (exclusiveOwner == FiberScheduler.thisFiberHandle());
    }

    void releaseExclusive() nothrow @nogc {
        assert (exclusiveOwner == FiberScheduler.thisFiberHandle(), "Only owner can release");
        assert (sharedOwners == 0);
        exclusiveOwner = null;
        _release();
    }

    void _release() nothrow @nogc {
        assert (!exclusiveOwner.isValid());
        assert (sharedOwners == 0);
        while (!waiters.empty()) {
            Ownership* ownership = cast(Ownership*)waiters.peekOpaque();
            ownership.granted = true;
            if (ownership.exclusive) {
                exclusiveOwner = waiters.resumeOne();
                break;
            }
            else {
                sharedOwners++;
                waiters.resumeOne();
            }
        }
    }

    static struct Acquisition {
        private RWLock* rwlock;
        private bool isExclusive;
        @disable this(this);
        ~this() nothrow @nogc {
            if (rwlock !is null) {
                if (isExclusive) {
                    rwlock.releaseExclusive();
                }
                else {
                    rwlock.releaseShared();
                }
                rwlock = null;
            }
        }
    }

    Acquisition sharedAcquisition() @nogc {
        acquireShared();
        return Acquisition(&this, false);
    }

    Acquisition exclusiveAcquisition() @nogc {
        acquireExclusive();
        return Acquisition(&this, true);
    }
}


