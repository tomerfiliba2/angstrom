module main;

import std.stdio;
import angstrom.logging;
import angstrom.reactor;


void main() {
/+
    Reactor.open(10, 16*1024);
    scope(exit) Reactor.close();

    Reactor.spawn(() {
        foreach (i; 0 .. 10) {
            writeln("A ", i);
            Reactor.yield();
            auto x = new ubyte[10000];
        }
        throw new Exception("kaki");
    });

    Reactor.spawn(() {
        foreach (i; 0 .. 20) {
            writeln("B ", i);
            Reactor.yield();
            auto x = new ubyte[10000];
        }
        //throw new Exception("kaki");

        Reactor.stop();
    });

    //try {
    Reactor.start();
    //}
    //catch (Exception ex) {
    //    INFO!"main caught %s"(ex.msg);
    //}
    writeln("main done");
+/
}



